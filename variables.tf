variable "name" {
  description = "Nome único para setar nos serviços"
}

variable "ami_id" {
  description = "Id da imagem (aMI)"
  default = "ami-0cbe40dd412e5ef32"
}

variable "instance_type" {
  description = "Tipo de instancia"
  default = "t2.micro"
}

variable "subnets" {
  type = list(string)
  description = "IDs das subnets"
}

variable "security_groups" {
  type = list(string)
  description = "Security groups"
}

variable "max_size" {
  description = "Número máximo de instancias"
  default = 1
}

variable "min_size" {
  description = "Número mínimo de instancias"
  default = 1
}
variable "desired_capacity" {
  description = "Número de instancias"
  default = 1
}

variable "volume_size" {
  description = "Tamanho do volume da instancia (GB)."
  default = "50"
}

variable "iam_instance_profile" {
  description = "Profile do IAM para a instancia"
}

variable "key_name" {
  description = "Nome da chave da instancia (pem)"
}

variable "cluster" {
  description = "Nome do cluster"
}

