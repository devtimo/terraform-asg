module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"
  
  name = var.name

  # Launch configuration
  lc_name = "${var.name}-lc"

  image_id        = var.ami_id
  instance_type   = var.instance_type
  security_groups = var.security_groups
  iam_instance_profile = var.iam_instance_profile

  root_block_device = [
    {
      volume_size = var.volume_size
      volume_type = "gp2"
    },
  ]
  key_name = var.key_name

  user_data = data.template_file.user_data.rendered

  # Auto scaling group
  asg_name                  = "${var.name}-asg"
  vpc_zone_identifier       = var.subnets
  health_check_type         = "ELB"
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_capacity
  wait_for_capacity_timeout = 0
}

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")

  vars = {
      cluster = var.cluster
  }
}
